'use strict';


var ArgumentParser = require('argparse').ArgumentParser;
var parser = new ArgumentParser({
    version: '0.0.1',
    addHelp: true,
    description: 'generates all possible permutations of a given string'
});
parser.addArgument(['-s', '--string'],
		   {help: 'string to generate permutations of'});
var args = parser.parseArgs();


function* permutationsGenerator(string) {
    if (string.length <= 1)
	yield string;
    else {
	for (let i = 0; i < string.length; i++) {
	    for (let permutation of permutationsGenerator(string.slice(0, i) + string.slice(i+1)))
	    {
	    	yield [string[i]].concat(permutation);
	    }
	}
    }
};


function generatePermutations(string) {
    for (let permutation of permutationsGenerator(string)) {
	if (string.length > 1)
	    console.log(permutation.join(''));
	else
	    console.log(permutation);
    }
};


generatePermutations(args.string);
